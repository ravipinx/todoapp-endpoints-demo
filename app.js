const express = require('express');
const cors = require('cors');
const CONFIG = require('./configs');
const mongoose = require("mongoose");
const http = require("http");
const routes = require("./routes");

const { onError, onListening } = require('./utils/functions');

// Database connection
const db_url = `${CONFIG.db_dialect}://${CONFIG.db_host}:${CONFIG.db_port}/${CONFIG.db_name}`;
mongoose.connect(db_url, { useUnifiedTopology: true, useNewUrlParser: true }).catch((err) => {
  console.log("MongoDB database connection failed");
  console.log(err);
});
mongoose.connection.once("open", function () {
  console.log(`MongoDB database connection successfully established on port: ${CONFIG.db_host}:${CONFIG.db_port}`);
});

// Initiate the app
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Load API routes
app.use("/api", routes);

app.use("/", function (req, res) {
  res.statusCode = 200; // send the appropriate status code
  res.json({ message: "Welcome to Todo App task" });
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.set("port", CONFIG.port);

// Create HTTP server.
const server = http.createServer(app);

// Listen on provided port, on all network interfaces.
server.listen(CONFIG.port);
// server.on("error", onError);
// server.on("listening", onListening(server));

