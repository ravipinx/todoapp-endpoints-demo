const httpStatus = require("http-status");
const { todoMaster } = require("../models");

const createTodo = async (req, res) => {
  const data = req.body; 
  const taskName  = req.body.taskName.trim();
  req.body.taskName = taskName;
  let query = { taskName };

  await todoMaster.findOne(query)
    .then(async (todo) => {
      if (!todo) {
        const todo = await todoMaster.create(data);
       
        return res.json({
          code: 200,
          status: true,
          message: "Record created",
          data: todo
        })
      } else {
        return res.json({
          code: 409,
          status: false,
          message: 'Already Exist!',
          data: {}
        })
      }
    })
    .catch((error) => {
      console.log(error);
      return res.json({
        code: 404,
        status: false,
        data: {}
      })
    });
};

const fetchAllTodos =  async (req, res) => {
  const skip = req.body.skip|0;
  const take = req.body.take|10;

  return await todoMaster.find().skip(skip).limit(take)
    .then(async (data) => {
      return await res.status(httpStatus.OK).send({ data });
    })
    .catch((error) => {
      return error;
    });
}

const deleteTodoFromDB = async (req, res) => {
  try {
    const { id } = req.params;
    todoData = await todoMaster.findByIdAndRemove(id);
    if (!todoData) {
      return res.json({
        code: 404,
        status: false,
        message: 'Invalid ID',
        data: {}
      })
    }
    return res.json({
      code: 200,
      status: true,
      message: 'Deleted!',
      data: {}
    })
  } catch {
    return res.json({
      code: 403,
      status: false,
      message: 'Invalid ID',
      data: {}
    })
  }
}

module.exports = {
  createTodo,
  fetchAllTodos,
  deleteTodoFromDB
}