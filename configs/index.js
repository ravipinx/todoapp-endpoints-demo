require("dotenv").config(); //instatiate environment variables

const CONFIG = {
  
  port: process.env.PORT || '4001',

  db_dialect: process.env.DB_DIALECT || "mongodb",
  db_host: process.env.DB_HOST || "localhost",
  db_port: process.env.DB_PORT || "27017",
  db_name: process.env.DB_NAME || "todoapp_db",
}

module.exports = CONFIG;
