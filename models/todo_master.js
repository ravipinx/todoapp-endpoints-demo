const mongoose = require("mongoose");

const todoMasterSchema = mongoose.Schema(
  {
    taskName: {
      type: String,
      require: true
    }
  },
  {
    timestamps: true,
  }
);

/**
 * @typedef Todo_Master
 */

const Todo_Master = mongoose.model("todo_master", todoMasterSchema);
module.exports = Todo_Master;