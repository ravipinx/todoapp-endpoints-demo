const express = require("express");
const router = express.Router();

const todoController = require("../controllers/todoController");

router.post("/todo/create", todoController.createTodo);
router.get("/todos", todoController.fetchAllTodos);
router.delete("/todo/delete/:id", todoController.deleteTodoFromDB);

module.exports = router;
